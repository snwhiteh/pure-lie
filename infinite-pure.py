# Since we have W = S_n invariance, a representative of a W-orbit can be described by its diagonal entries in no particular order,
# or by the set of entries in its diagonal. For an element of order p, these are all of the form zeta^k for 0 <= k < p (and if p is even,
# an additional element consisting of zeta_{2p}^{p/2}*I (which has square -I = I in SL_n/{+- I})
# Thus a representative can equally well be described by the number of times zeta^k occurs for each k. Let k_i be the number of times \zeta^i occurs.
# Then k_0 + ... + k_{p-1} = n, and 0 k_0 + 1 k_1 + ... + (p-1)k_{p-1} = 0 mod p.
# There should be a nice way to write the dimension of the space left invariant by t in terms of the k_i
# Our basic tools are the standard representation, direct sums, tensors, exterior powers, and symmetric powers. Each of these can be handled individually.

# Fix an order for our elements t
p = 2

# Set up some variables to work with; k = k_1 and k_0 = n-k_1 = n-k
n, k = var('n, k')

def V():
    return [n-k, k]

def exterior(l, v):
    ret = [0, 0]
    # the +1 eigenspace means we need to choose an even number of entries in the k
    # the -1 eigenspace is an odd number of entries in the k
    # we can choose anywhere from 0 to maybe l entries in the k
    for i in range(0, l+1):
        if i % 2 == 0:
            ret[0] += binomial(k, i) * binomial(n-k, l-i)
        else:
            ret[1] += binomial(k, i) * binomial(n-k, l-i)
    return ret
        

def symmetric(l, v):
    if l != 2:
        print("Not implemented yet")
    
    # if v_1, ..., v_f(k,n) is a +1 eigenbasis for V and
    #    w_1, ..., w_g(k,n) is a -1 eigenbasis for V, then
    # the basis for S2(V) contains terms of the form
    # v_i v_j in the +1 eigenbasis -- there are ((f(k,n)+1) choose 2) of these
    # w_i w_j in the +1 eigenbasis -- there are ((g(k,n)+1) choose 2) of these
    # v_i w_j in the -1 eigenbasis -- there are f(k,n)g(k,n) = ((f(k,n) + g(k,n) + 1) choose 2) - ((f(k,n)+1) choose 2) - ((g(k,n) + 1) choose 2) of these
    return [ binomial(v[0]+1, 2) + binomial(v[1]+1, 2), v[0] * v[1] ]

def tensor(v, w):
    return [ v[0] * w[0] + v[1] * w[1], v[1] * w[0] + v[0] * w[1] ]

def sum(v, w):
    return [ v[0] + w[0], v[1] + w[1] ]

def quotient(v, w):
    return [ v[0] - w[0], v[1] - w[1] ]

# construct your representation here:
# 2e_1 - e_n => 3.78
#R = quotient(tensor(symmetric(2, V()), V()), V())
# 2e_1 + e_2 => 6.64
#R = quotient(tensor(V(), exterior(2, V())), exterior(3, V()))
# e_1 + e_2 + e_3 => 15.39
R = exterior(3, V())
# 2e_1 + 2e_2 => this analysis needs to be checked further; doesn't seem to work nicely
#R = quotient(symmetric(2, exterior(2, V())), exterior(4, V()))
# 3e_1 => symmetric cubes are not yet implemented
#R = symmetric(3, V())

# f(k,n) is the dimension of the -1 eigenspace of the action of t_k on R
f = simplify(expand(R[1]))

# find where we intersect dim(n)+2 at each endpoint k=2, k=n, as well as anywhere where the
# partial derivative w.r.t. k vanishes
solns = solve(diff(f,k), k, solution_dict=True)

possible_minima = [2, *[ s[k] for s in solns ], n]
worst = 0

for point in possible_minima:
    if point == 0:
        continue
    
    print(simplify(expand(f(point,n)-(n*n-1+2))))
    root = 0
    while True:
        try:
            root = find_root(f(point,n) - (n*n - 1 +2), root+0.1, 100)
        except:
            break
    worst = max(worst, root)

print(worst)
