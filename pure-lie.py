import subprocess
import tempfile
import sys
import itertools

default_rs = "A3"
def get_default_rs():
    return default_rs

def set_default_rs(rs):
    global default_rs
    default_rs = rs

def invoke_lie(cmd):
    with tempfile.TemporaryFile() as f: 
        f.write(bytes(cmd, encoding='utf-8'))
        f.seek(0)
        c = subprocess.run(["/home/phi/LiE/Lie.exe"], stdin=f, capture_output=True)
        return c.stdout.decode('utf-8').strip()

def strip_brackets(x):
    return x[x.find('[')+1:x.find(']')]

def dim(x):
    s = f"{x}"
    if s[0] != '[':
        n = int(s[1:])
        if s[0] == 'A':
            return (n+1)*(n+1)-1
    return int(invoke_lie(f"dim({s}, {get_default_rs()})"))

def spectrum(l, t, rs=None):
    if rs is None:
        rs = get_default_rs()
    s = f"spectrum({l}, {t}, {rs})"
    output = invoke_lie(s)
    return { int(strip_brackets(x[1])) : (int(x[0]), int(t[-1])) for x in [ y.split('X') for y in output.split('+') ] }

def reps(order, rs):
    r = []
    if order == 2 and rs[0] == 'A':
        n = int(rs[1:])
        for i in range((n+1)//2):
            o = 2 if (i+1)*2 != n+1 else 4
            l = []
            for j in range(n):
                if j % 2 == 0 and j <= 2*i:
                    l.append(1)
                else:
                    l.append(0)
            l.append(o)
            r.append(l)
    elif order % 2 == 1 and rs[0] == 'A':
        n = int(rs[1:])
        all_elements = itertools.product(*[ [ x for x in range(order) ] for y in range(n) ])
        keep = {}
        for element in all_elements:
            if element[0] == 0:
                continue
            degs = []
            degs.append(element[0])
            for i in range(len(element)-1):
                degs.append((element[i+1]-element[i]+2*order)%order)
            key = ','.join([ str(x) for x in sorted(degs)])
            if key in keep:
                continue
            keep[key] = [*element, order]
        return [ v for (k,v) in keep.items() ]

                
    return r

def is_pure(l, rs):
    if len(rs) == 1:
        rs = f"{rs}{len(l)}"
    set_default_rs(rs)
    if len(l) != int(rs[1:]):
        print("Highest weight vector has wrong length for root system")
        exit(1)

    for order in [2, 3]:
        spectra = [ spectrum(l, t, rs) for t in reps(order, rs) ]
        print(spectra, dim(l)-dim(rs)-2)
        if max([ x[0][0] for x in spectra if 0 in x ]) <= dim(l) - dim(rs) - 2:
            return False

    return True

def main():
    if len(sys.argv) > 1 and sys.argv[1][0] == '[':
        l = eval(sys.argv[1])
        rs = sys.argv[2]
        if not is_pure(l,rs):
            print(f"The representation with highest weight vector {l} is not pure")
        else:
            print(f"The representation with highest weight vector {l} might be pure")
    else:
        ls = []
        rss = []
        for line in sys.stdin:
            if line[0] == '#':
                continue
            l = eval(line.strip().split(" ")[0])
            rs = line.strip().split(" ")[1]
            ls.append(l)
            rss.append(rs)

        k = []
        for (l, rs) in zip(ls, rss):
            if not is_pure(l,rs):
                print(f"The representation with highest weight vector {l} is not pure")
            else:
                print(f"The representation with highest weight vector {l} might be pure")
                k.append((l, rs))

        if len(k) != 0:
            print("The following representations might be pure: ")
            for (l,rs) in k:
                print(f"{l} {rs}")
        else:
            print("None of those representations were pure.")

if __name__=='__main__':
    main()
